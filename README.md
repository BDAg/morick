# Morick - Loja e-commerce

### Documentação

- [Equipe](https://gitlab.com/BDAg/morick/-/wikis/Equipe)
- [MVP](https://gitlab.com/BDAg/morick/-/wikis/MVP)
- [Cronograma](https://gitlab.com/BDAg/morick/-/wikis/Cronograma)
- [Mapa de Conhecimento](https://gitlab.com/BDAg/morick/-/wikis/Mapa-de-Conhecimento)
- [Matriz de Habilidades](https://gitlab.com/BDAg/morick/-/wikis/Matriz-de-Habilidades)

### Instalação

- [Instalação Wordpress](https://gitlab.com/BDAg/morick/-/wikis/Instalação-Wordpress)
- [Integração WooCommerce](https://gitlab.com/BDAg/morick/-/wikis/Integração-WooCommerce)
- [Método de Envio, Gerenciamento de Estoque e Métodos de Pagamento](https://gitlab.com/BDAg/morick/-/wikis/Método-de-Envio-e-Pagamento)

### Documentação Final

- [Matriz de Habilidades - Final](https://gitlab.com/BDAg/morick/-/wikis/Matriz-de-Habilidades---Final)
- [Solução Final](https://gitlab.com/BDAg/morick/-/wikis/Documentação---Solução-Final)
- [Apresentação Final](https://gitlab.com/BDAg/morick/-/wikis/Apresentação-Final)
